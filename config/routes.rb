Rails.application.routes.draw do
  devise_for :users
  resources :docs
  get 'welcome/index'

  resources :doc
  authenticated :user do
    root to: 'docs#index', as: 'authenticated_root'
  end

  root to: 'welcome#index'
end
