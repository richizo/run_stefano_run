
#### [Current]

#### 
 * [6eea8e9](../../commit/6eea8e9) - __(Ricardo Ichizo)__ Adding the Hierapolis Admin Dashboard
 * [5fa5dca](../../commit/5fa5dca) - __(Ricardo Ichizo)__ Adding Rails structure
 * [f3c10b0](../../commit/f3c10b0) - __(Ricardo Ichizo)__ Update README.md
 * [e1993a1](../../commit/e1993a1) - __(Ricardo Ichizo)__ Add new file
 * [4d9b520](../../commit/4d9b520) - __(Ricardo Ichizo)__ Add license
